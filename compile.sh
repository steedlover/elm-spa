#!/bin/sh

set -e

src=$1
in="$2.elm"
dist="dist"
out="$2.js"
min="$2.min.js"
env='production'

if [ -n $1 ]; then
  param="$(echo $3 | cut -d'=' -f1)"
  value="$(echo $3 | cut -d'=' -f2)"

  if [ ! -z "$param" -a "$param" = "env" ]; then
    if [ ! -z "$value" -a "$value" = "dev" ]; then
      env="development"
    fi
  fi
fi

if [ "$env" = "production" ]; then
  elm make "$src/$in" --optimize --output="$dist/$out"

  uglifyjs "$dist/$out" --compress "pure_funcs=[F2,F3,F4,F5,F6,F7,F8,F9,A2,A3,A4,A5,A6,A7,A8,A9],pure_getters,keep_fargs=false,unsafe_comps,unsafe" | uglifyjs --mangle --output="$dist/$min"

  echo "Initial size: $(cat "$src/$in" | wc -c) bytes  ($src/$in)"
  echo "Minified size:$(cat "$dist/$min" | wc -c) bytes  ($dist/$min)"
  echo "Gzipped size: $(cat "$dist/$min" | gzip -c | wc -c) bytes"
else
  elm make "$src/$in" --output="$dist/$out"
fi
