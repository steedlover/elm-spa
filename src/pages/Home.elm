module Home exposing (Model, Msg, init, update, view)

import Html exposing (Html, div, h2, p, section, text)
import Html.Attributes exposing (class)


type alias Model =
    { text : String
    , title : String
    }


type Msg
    = NoOp


init : ( Model, Cmd Msg )
init =
    ( { text = "Hello world!", title = "Home page" }, Cmd.none )


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        NoOp ->
            ( model, Cmd.none )


view : Model -> Html Msg
view model =
    section []
        [ h2 [] [ text model.title ]
        , text model.text
        ]
