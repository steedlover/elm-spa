module About exposing (Model, Msg, init, update, view)

--import Html.Attributes exposing (class)
--import Html.Events exposing (custom)
--import Json.Decode exposing (map, succeed)

import Bootstrap.Button as Button
import Bootstrap.Utilities.Spacing as Spacing
import Html exposing (Attribute, Html, div, h2, p, section, text)
import Html.Events exposing (onClick, preventDefaultOn)


type alias Model =
    { text : String
    , title : String
    }


type Msg
    = NoOp
    | ButtonClick


init : ( Model, Cmd Msg )
init =
    ( { text = "Hello about page!", title = "About page" }, Cmd.none )


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        NoOp ->
            ( model, Cmd.none )

        ButtonClick ->
            ( { model | text = "Not so fun (" }, Cmd.none )


view : Model -> Html Msg
view model =
    section []
        [ h2 [] [ text model.title ]
        , div [] [ text model.text ]
        , Button.button
            [ Button.primary
            , Button.attrs
                [ Spacing.mt2
                , onClick ButtonClick

                --, preventDefaultOn "click"
                --    <| succeed (ButtonClick, True)
                --, custom "click"
                --    <| succeed { message = ButtonClick, stopPropagation = True, preventDefault = True }
                ]
            ]
            [ text "Click for fun" ]
        ]
