module Route exposing (Route(..), parseUrl)

import Url exposing (Url)
import Url.Parser as Parser exposing ((</>), Parser, oneOf, parse, s)



-- ROUTING


type Route
    = NotFound
    | Home
    | About
    | Login
    | Logout


parseUrl : Url -> Route
parseUrl url =
    case parse matchRoute url of
        Just route ->
            route

        Nothing ->
            NotFound


matchRoute : Parser (Route -> a) a
matchRoute =
    oneOf
        [ Parser.map Home Parser.top
        , Parser.map Home (s "home")
        , Parser.map About (s "about")
        , Parser.map Login (s "login")
        , Parser.map Logout (s "logout")
        ]
