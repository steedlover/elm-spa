module Main exposing (..)

-- Import pages

import About
import Bootstrap.Grid as Grid
import Bootstrap.Grid.Col as Col
import Bootstrap.Grid.Row as Row
import Bootstrap.Navbar as Navbar
import Browser
import Browser.Navigation as Nav
import Home
import Html exposing (Html, a, b, div, h2, li, p, text, ul)
import Html.Attributes exposing (class, href)
import Html.Lazy exposing (lazy)
import Route exposing (Route)
import Url



-- MAIN


main : Program () Model Msg
main =
    Browser.application
        { init = init
        , view = view
        , update = update
        , subscriptions = subscriptions
        , onUrlChange = UrlChanged
        , onUrlRequest = LinkClicked
        }



-- MODEL


type alias Model =
    { route : Route
    , page : Page
    , navKey : Nav.Key
    , navbarState : Navbar.State
    }


type Page
    = NotFoundPage
    | HomePage Home.Model
    | AboutPage About.Model


init : () -> Url.Url -> Nav.Key -> ( Model, Cmd Msg )
init flags url key =
    let
        ( navbarState, navCmd ) =
            Navbar.initialState NavbarMsg

        model =
            { route = Route.parseUrl url
            , page = NotFoundPage
            , navKey = key
            , navbarState = navbarState
            }
    in
    initCurrentPage ( model, navCmd )


initCurrentPage : ( Model, Cmd Msg ) -> ( Model, Cmd Msg )
initCurrentPage ( model, existingCmds ) =
    let
        ( currentPage, mappedPageCmds ) =
            case model.route of
                Route.NotFound ->
                    ( NotFoundPage, Cmd.none )

                Route.Home ->
                    let
                        ( pageModel, pageCmds ) =
                            Home.init
                    in
                    ( HomePage pageModel, Cmd.map HomePageMsg pageCmds )

                Route.About ->
                    let
                        ( pageModel, pageCmds ) =
                            About.init
                    in
                    ( AboutPage pageModel, Cmd.map AboutPageMsg pageCmds )

                Route.Login ->
                    ( NotFoundPage, Cmd.none )

                Route.Logout ->
                    ( NotFoundPage, Cmd.none )
    in
    ( { model | page = currentPage }
    , Cmd.batch [ existingCmds, mappedPageCmds ]
    )



-- UPDATE


type Msg
    = LinkClicked Browser.UrlRequest
    | UrlChanged Url.Url
    | HomePageMsg Home.Msg
    | AboutPageMsg About.Msg
    | NavbarMsg Navbar.State


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case ( msg, model.page ) of
        ( NavbarMsg state, _ ) ->
            ( { model | navbarState = state }, Cmd.none )

        ( HomePageMsg subMsg, HomePage pageModel ) ->
            let
                ( updatedPageModel, updatedCmd ) =
                    Home.update subMsg pageModel
            in
            ( { model | page = HomePage updatedPageModel }
            , Cmd.map HomePageMsg updatedCmd
            )

        ( AboutPageMsg subMsg, AboutPage pageModel ) ->
            let
                ( updatedPageModel, updatedCmd ) =
                    About.update subMsg pageModel
            in
            ( { model | page = AboutPage updatedPageModel }
            , Cmd.map AboutPageMsg updatedCmd
            )

        ( LinkClicked urlRequest, _ ) ->
            case urlRequest of
                Browser.Internal url ->
                    ( model
                    , Nav.pushUrl model.navKey (Url.toString url)
                    )

                Browser.External url ->
                    ( model
                    , Nav.load url
                    )

        ( UrlChanged url, _ ) ->
            let
                newRoute =
                    Route.parseUrl url
            in
            ( { model | route = newRoute }, Cmd.none ) |> initCurrentPage

        ( _, _ ) ->
            ( model, Cmd.none )



-- SUBSCRIPTIONS


subscriptions : Model -> Sub Msg
subscriptions model =
    Navbar.subscriptions model.navbarState NavbarMsg



-- VIEW


view : Model -> Browser.Document Msg
view model =
    { title = pageTitle model.page
    , body =
        [ lazy renderNavBar model.navbarState
        , Grid.container [ class "content" ]
            [ Grid.row []
                [ Grid.col [ Col.xs12 ]
                    [ lazy renderPage model.page ]
                ]
            ]
        ]
    }


pageTitle : Page -> String
pageTitle page =
    let
        title =
            case page of
                NotFoundPage ->
                    "Page not found"

                HomePage pageModel ->
                    pageModel.title

                AboutPage pageModel ->
                    pageModel.title
    in
    title


renderNavBar : Navbar.State -> Html Msg
renderNavBar state =
    div [ class "nav-wrapper" ]
        [ Navbar.config NavbarMsg
            |> Navbar.withAnimation
            |> Navbar.container
            |> Navbar.brand [ href "/" ] [ text "Brand" ]
            |> Navbar.items
                [ navbarLink "/about"
                ]
            |> Navbar.view state
        ]


renderPage : Page -> Html Msg
renderPage page =
    case page of
        NotFoundPage ->
            notFoundView

        HomePage pageModel ->
            Home.view pageModel
                |> Html.map HomePageMsg

        AboutPage pageModel ->
            About.view pageModel
                |> Html.map AboutPageMsg


notFoundView : Html Msg
notFoundView =
    p [ class "lead" ] [ text "not found" ]


navbarLink : String -> Navbar.Item msg
navbarLink path =
    Navbar.itemLink [ href path ] [ text path ]
